class Filter {
  constructor(options) {
    this.filterType = options.filterType
    this.parentClass = options.element
    this.listElement = document.querySelector(options.element)
    this.collection = Array.from(document.querySelectorAll(options.element + '> *'));
    this.filterSet = [];

    options.filters.forEach((filter) => {
      let obj = {
        type: filter.filterType,
        filterSelect: document.querySelector(filter.filterSelect),
        currentValue: 'all'
      }

      this.filterSet.push(obj);
    })

    this.init();
  }

  init() {
    this.filterSet.forEach((filter) => {
      this.buildFilterSelect(filter.filterSelect, this.collection, filter.type)
      filter.filterSelect.addEventListener('change', this.filter.bind(this));
    })
  }

  filter(e) {
    
    // update filters state
    this.filterSet.forEach((item, i) => {
      if (item.type == e.target.getAttribute('name')) {
        this.filterSet[i].currentValue = e.target.value
      }
    })
 
    // empty dom
    while(this.listElement.firstChild) {
      this.listElement.removeChild(this.listElement.firstChild);
    }

    // filter collection
    let recursive_filter = (data, filtersArray, index = 0) => {

      let compare = (x, y) => {
        if (x == 'all' || x == y) return true 
      }
    
      // if there are no filters being checked, send back an unfiltered list
      if (filtersArray.length === 0) {
        return data
      }
    
      // if we are checking the last filter, filter and return list
      if (index === filtersArray.length - 1) {
        return data.filter(
          (item) => {
            let x = item.getAttribute('data-' + this.filterSet[index].type)
            return compare(this.filterSet[index].currentValue, x)
          }
        )
      }
    
      // recur
      return recursive_filter(data.filter(
        (item) => {
          let x = item.getAttribute('data-' + this.filterSet[index].type)
          return compare(this.filterSet[index].currentValue, x)
        }
      ), filtersArray, (index + 1))
    }
    
    // repopulate dom with filtered content
    recursive_filter(this.collection, this.filterSet).forEach((el) => {
      this.listElement.appendChild(el);
    })
  }

  // populate select input elements with filter options
  buildFilterSelect(selectEl, collection, filterType) {

    // return array of unique filters
    let unique = (arr, filterType) => {
      let x = [];
      x.push('all');
      arr.forEach((item) => {
        let category = item.getAttribute('data-' + filterType)
        if (!x.includes(category)) {
          x.push(category)
        } 
      })
  
      return x
    }

    // populate select element with filters
    let filters = unique(this.collection, filterType);
    filters.forEach((category) => {
      let option = document.createElement('option');
      option.setAttribute('value', category)
      option.innerText = category;
      selectEl.appendChild(option)
    })
  }
}

export default Filter