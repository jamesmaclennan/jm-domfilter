# jm-domfilter

a simple class that filters a list of dom elements by a specified data attribute.

## usage

    let filters = new Filter({
      element: '.list',
      filters: [
        {
          filterSelect: '.select-filter-league',
          filterType: 'league'
        },
        {
          filterSelect: '.select-filter-division',
          filterType: 'division'
        }
      ]
    })

### options.element
class name of parent element that contains a collection of items to be filtered.

### options.filterType 
data attribute by which to filter elements.

### options.filterSelect
input select element to be populated with filter names.
